#!/usr/bin/env python
# -*- coding: utf-8 -*-

import gensim
from gensim.models import KeyedVectors

model = KeyedVectors.load("model_events.kv")

a = input()
def print_similiar(name):
    for node, _ in model.wv.most_similar(name):
        print(node)

print_similiar(a)
