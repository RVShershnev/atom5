﻿using Atom.Culture.App.Data.Interfaces;
using Atom.Culture.App.Data.Models;
using Atom.CultureShared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Atom.Culture.PublishServer.Services
{
    public class ManageService
    {
        IUnitOfWork unitOfWork;
        static Random random = new Random();
        MlService _MlService;
        public ManageService(IUnitOfWork unitOfWork, MlService mlService)
        {
            this.unitOfWork = unitOfWork;
            _MlService = mlService;
        }

        public async Task<Atom.CultureShared.Person> UpdatePerson(Atom.CultureShared.Person message)
        {
            var a = message;
            return a;
        }
        public async Task<Atom.CultureShared.Person> FindPerson(string id)
        {
            // если пользователь не найден, то 
            bool IsFound = false;                        
            if (true)
            {
                IsFound = true;
                Atom.CultureShared.Person person = new Atom.CultureShared.Person();
                return person;
            }
            return null;
        }

        public async Task<IEnumerable<string>> FamousTags(string id)
        {
            List<string> tags = new List<string>();

            // если пользователь не найден, то 
            bool IsFound = false;
            if (true)
            {
                IsFound = true;
                Atom.CultureShared.Person person = new Atom.CultureShared.Person();
               
            }
            return null;
        }

        public async Task<IEnumerable<string>> GetMetro(string id)
        {
            return Metro.GetMetro;                        
        }

        public async Task<Atom.CultureShared.Person> RandomPersonRecomendation(Atom.Culture.App.Data.Models.Person person)
        {
            var recomendation = new List<IRecomendation>();
            var nameBook = GetTheBestBookFromPerson(person); // из базы.
            List<Culture.App.Data.Models.Book> recomendationBooks = new List<Culture.App.Data.Models.Book>();
            List<Culture.App.Data.Models.Event> recomendationEvents = new List<Culture.App.Data.Models.Event>();
            List<Culture.App.Data.Models.Service> recomendationServices = new List<Culture.App.Data.Models.Service>();
            if (nameBook == null)
            {
                // Сказать, что он ещё не читал книги и дать рандомные
                recomendationBooks = GetRandomBooks();
            }
            var newNameBooks = _MlService.BookModel(nameBook);
            foreach (var item in newNameBooks)
            {
                if (item != "")
                {
                    var book = unitOfWork.Books.Where(x => x.Name == item).First();
                    recomendationBooks.Add(book);
                }
            }         
            if(recomendationBooks.Count == 0)
            {
                recomendationBooks = GetRandomBooks();
            }
            foreach (var item in recomendationBooks)
            {
                Atom.CultureShared.Book rbook = new CultureShared.Book()
                {
                    Id = item.DocId,
                    Name = item.Name
                };
                recomendation.Add(rbook);
            }

            var eventName = unitOfWork.Events.Where(x => x.EvId == random.Next(0, 100).ToString()).First();
            var newNameEvents = _MlService.EventModel(eventName.Name);
            if (newNameEvents != null && newNameEvents.Count() > 0)
            {
                foreach (var item in newNameEvents)
                {
                    if (item != "")
                    {                      
                           
                            Atom.CultureShared.Activity rbook = new CultureShared.Activity()
                            {                                
                                Name = item
                            };
                            recomendation.Add(rbook);                     
                    }
                }
            }
            else
            {
                recomendationEvents = GetRandomEvent();
            }

            //var serviceName = unitOfWork.Services.Where(x => x.ServId == random.Next(0, 100).ToString()).First();
            //var newNameservice = _MlService.ServicesModel(person);
            //if (newNameEvents != null && newNameEvents.Count() > 0)
            //{
            //    foreach (var item in newNameEvents)
            //    {
            //        var ttt = unitOfWork.Services.Where(x => x.Classificator == item).First();
            //        Atom.CultureShared.Activity rbook = new CultureShared.Activity()
            //        {
            //            Id = ttt.ServId,
            //            Name = ttt.ServiceName
            //        };
            //        recomendation.Add(rbook);
            //    }
            //}
            //else
            //{
            //    recomendationServices = GetRandomService();
            //}


            var result = new Atom.CultureShared.Person()
            {
                Id = person.Id                
            };
            result.Recomendations = recomendation;
            return result;
        }

        public async Task<Atom.CultureShared.Group> RandomGroupRecomendation(Atom.CultureShared.Group group)
        {
            var recomendation = new List<IRecomendation>();
            return group;
        }


        private List<Culture.App.Data.Models.Book> GetRandomBooks()
        {
            var ss = new List<Atom.Culture.App.Data.Models.Book>();
                
            int booksCount = random.Next(3, 6);
            var r1 = random.Next(1, 500).ToString();
            var r2 = random.Next(500, 1000).ToString();
            var r3 = random.Next(1500, 2000).ToString();
            var book1 = unitOfWork.Books.Where(x => x.DocId == r1).First();
            var book2 = unitOfWork.Books.Where(x => x.DocId == r2).First();
            var book3 = unitOfWork.Books.Where(x => x.DocId == r3).First();
            ss.Add(book1);
            ss.Add(book2);
            ss.Add(book3);
            return ss;
        }
        private List<Culture.App.Data.Models.Event> GetRandomEvent()
        {
            var ss = new List<Atom.Culture.App.Data.Models.Event>();

            int booksCount = random.Next(3, 6);
            var r1 = random.Next(1, 500).ToString();
            var r2 = random.Next(500, 1000).ToString();
            var r3 = random.Next(1500, 2000).ToString();
            var book1 = unitOfWork.Events.Where(x => x.EvId == r1).First();
            var book2 = unitOfWork.Events.Where(x => x.EvId == r2).First();
            var book3 = unitOfWork.Events.Where(x => x.EvId == r3).First();
            ss.Add(book1);
            ss.Add(book2);
            ss.Add(book3);
            return ss;
        }

        private List<Culture.App.Data.Models.Service> GetRandomService()
        {
            var ss = new List<Atom.Culture.App.Data.Models.Service>();

            int booksCount = random.Next(3, 6);
            var r1 = random.Next(1, 500).ToString();
            var r2 = random.Next(500, 1000).ToString();
          
            var book1 = unitOfWork.Services.Where(x => x.ServId == r1).First();
            var book2 = unitOfWork.Services.Where(x => x.ServId == r2).First();
         
            ss.Add(book1);
            ss.Add(book2);
       
            return ss;
        }

        private string GetTheBestBookFromPerson(Culture.App.Data.Models.Person personFromDataSet)
        {
            string result = null;
            if (personFromDataSet.Books.Count >= 0)
            {
                result = personFromDataSet.Books.ElementAt(random.Next(0, personFromDataSet.Books.Count)).Name;
            }
            return result;
        }
    }
}
