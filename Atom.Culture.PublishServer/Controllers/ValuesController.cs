﻿using Atom.Culture.App.Data.Interfaces;
using Atom.Culture.App.Data.Models;
using Atom.Culture.PublishServer.Services;
using Atom.CultureShared;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson.IO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Atom.Culture.PublishServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        MlService _MlService;
        IUnitOfWork unitOfWork;
        ManageService _manageService;
        public ValuesController(IUnitOfWork unitOfWork, MlService mlService, ManageService manageService)
        {
            this.unitOfWork = unitOfWork;
            this._MlService = mlService;
            this._manageService = manageService;
        }

        // GET: api/<ValuesController>
        [HttpGet]
        public string Get()
        {
            return DateTime.Now.ToString();
        }

        // GET api/<ValuesController>/5
        [HttpGet("person/{id}")]
        public string GetPerson(string id)
        {
            // найти пользователя и выдать о нем информацию.
            Atom.Culture.App.Data.Models.Person personFromDataSet = unitOfWork.Persons.GetFromDataset(id); // 2569
            if (personFromDataSet == null)
            {
                return null; // читателя нет 
            }          
            var result = _manageService.RandomPersonRecomendation(personFromDataSet).Result;            
            var stringresult = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            return stringresult;


        }

        [HttpGet("group/{id}")]
        public string GetGroup(string ids)
        {
            // найти пользователя и выдать о нем информацию.
            Group group = new Group();
            group.Persons.Add(new Atom.CultureShared.Person() { Age = "11", Email = "12121" });
            //person.Age = "19";
            //person.Name = "Roman";
            var result = Newtonsoft.Json.JsonConvert.SerializeObject(group);
            return result;
        }
    }
}
